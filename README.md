# scrutiny-sandbox

Sandbox demonstrating the certification environment using https://github.com/crocs-muni/scrutiny

## Setup

Topology definition and basic configuration is available in [simple_scrutiny.yml](https://gitlab.fi.muni.cz/cybersec/cs4e/scrutiny-sandbox/-/blob/main/simple_scrutiny.yml) file.

Ansible playbook with Scrutiny setup is available in [playbook.yml](https://gitlab.fi.muni.cz/cybersec/cs4e/scrutiny-sandbox/-/blob/main/provisioning/playbook.yml) file.

Please refer to [Scrutiny_CSC.pdf](https://gitlab.fi.muni.cz/cybersec/cs4e/scrutiny-sandbox/-/blob/main/Scrutiny_CSC.pdf) for the detailed installation, setup and usage process.

## Note

Generated with [Cyber Sandbox Creator](https://gitlab.ics.muni.cz/muni-kypo-csc/cyber-sandbox-creator) version 2.1.0.